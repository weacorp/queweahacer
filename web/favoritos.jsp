<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <link rel="stylesheet" type="text/css" href="css/menu.css" />
        <link rel="stylesheet" type="text/css" href="css/FormatoTarjetaLista.css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Favoritos</title>
    </head>
    <%
        if (session.getAttribute("login") == null) {
            response.sendRedirect("login.jsp");
        }
    %>
    <body>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>
                </div>
                <div id="linksHead" class="percent centradoAbsoluto">
                    ${head} 
                </div>
            </div>
        </div>
    </div>
    <div id="contenedor" class="contenedor centrarHorizontal">
        <nav id="menu-lateral" class="menu-lateral">
            <button id="boton-menu"><img id="menu-icon" src="images/menuw.png" /></button>
            <nav class="menu-lateral">
                <a href="CrearPublicacion">Crear publicación</a>
                <a href="MisPublicaciones">Mis publicaciones</a>
                <a href="ListaFavoritos">Favoritos</a>
            </nav>
        </nav>
        <main id="content" class="main">
            <h3 class="tituloMain">Favoritos</h3>
            <ul class="items">${elementoLista}</ul>
            <div id="${status}">
                ${mensaje}
            </div>
        </main>
    </div>
    <footer id="footer" class="foodter">footer</footer>
    <script src="JS/home.js"></script>
    <script src="JS/jquery-3.5.1.slim.min.js" type="text/javascript"></script>
    <script src="JS/popper.min.js" type="text/javascript"></script>
    <script src="JS/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>