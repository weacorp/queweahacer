<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/signup.css">
        <link rel="stylesheet" type="text/css" href="css/head.css" />
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <title>SignUp</title>
    </head>

    <body>
        <%
            if (session.getAttribute("login") != null) {
                response.sendRedirect("index.jsp");
            }
        %>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>
                </div>
            </div>
        </div>
        <div>
            <div class="form-title">
                <h1>Reg��strate</h1>
            </div>

            <form class="signup-form" action="SignUpUser" method="POST">
                <div class="form-container">
                    <div class="data-container">
                        <div class="form-first">
                            <label for="name">Nombre(s):</label><br>
                            <input type="text" name="name" placeholder="Juan" required value="${name}"><br>
                            <label for="lastname">Apellidos</label><br>
                            <input type="text" name="lastname" placeholder="L�pez..." required value="${lastname}"><br>
                            <label for="email">Correo electr�nico</label><br>
                            <input type="email" name="email" placeholder="mail@mail.com" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${email}"><br>
                            <label for="email-confirm">Confirmar correo</label><br>
                            <input type="email" name="email-confirm" placeholder="Repita el email" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${emailC}"><br>
                        </div>
                        <div class="form-second">
                            <label for="tel">Tel�fono</label><br>
                            <input type="number" name="tel" placeholder="10 digitos" required max="9999999999" value="${tel}"><br>
                            <label for="tel-confirm">Confirmar tel�fono</label><br>
                            <input type="number" name="tel-confirm" placeholder="Repita el tel�fono" required max="9999999999" value="${telC}"><br>
                            <label for="password">Contrase�a</label><br>
                            <input type="password" name="password" placeholder="Ingrese su contrase�a" required value="${pass}"><br>
                            <label for="password-confirm">Confirmar contrase�a</label><br>
                            <input type="password" name="password-confirm" placeholder="Repita su contrase�a" required value="${passC}"><br>
                        </div>
                    </div>
                    <div class="buttons-row">
                        <div class="form-confirm">
                            <label class="check" for="terms"><input type="checkbox" name="terms" required>Acepto los t�rminos y
                                condiciones</label><br>
                            <button type="submit" value="SignUpUser">Crear cuenta</button><br>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div id="${status}">
            ${mensaje}
        </div>
        <footer id="footer" class="foodter">footer</footer>
        <script src="JS/home.js"></script>
    </body>

</html>