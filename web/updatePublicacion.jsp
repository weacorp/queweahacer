<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="css/head.css" />
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <link rel="stylesheet" type="text/css" href="css/armaTarjeta.css" />
        <link rel="stylesheet" type="text/css" href="css/crearPublicacion.css" />
        <link rel="stylesheet" type="text/css" href="css/menu.css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Actualizar Publicaci�n</title>
    </head>
    <%
        if (session.getAttribute("login") == null) {
            response.sendRedirect("login.jsp");
        }
    %>
    <body>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>
                </div>
                <div id="linksHead" class="percent centradoAbsoluto">
                    ${head}
                </div>
            </div>
        </div>
        <div id="contenedor" class="contenedor centrarHorizontal">
            <nav id="menu-lateral" class="menu-lateral">
                <button id="boton-menu"><img id="menu-icon" src="images/menuw.png" /></button>
                <nav class="menu-lateral">
                    <a href="CrearPublicacion">Crear publicaci�n</a>
                    <a href="MisPublicaciones">Mis publicaciones</a>
                    <a href="ListaFavoritos">Favoritos</a>
                </nav>
            </nav>
            <main id="content" class="main">
                <h3 class="tituloMain">Modificar Publicaci�n</h3>
                <form id="crear-publicacion" action="UpdatePublicacion" method="POST">
                    <div class="containerBody percent centrarHorizontal">
                        <div class="container percent divMultipleRow centrarVertical">
                            <div class="left divMultipleColumn percent overflowHidden">
                                <div class="descripcion percent">
                                    <h3 class="tituloCuerpo2">Breve Descripci�n</h3>
                                    <div id="breveDesc" class=" percent overflow scroller">
                                        <textarea name="descripcion" id="descripcion" cols="30" rows="10">${descripcion}</textarea>
                                    </div>
                                </div>
                                <div class="nombreProyecto percent">
                                    <h3 class="tituloCuerpo2">Nombre del proyecto</h3>
                                    <input type="text" name="nombreProyecto" id="nombreProyecto" value="${nombreProyecto}"/>
                                </div>
                                <div class="categoria percent">
                                    <h3 class="tituloCuerpo2">Categor�a</h3>
                                    <select name="categoriaX" id="categoriaX">
                                        <option ${P} value="Programación WEB" ${val1}>Programaci�n WEB</option>
                                        <option ${D} value="Dispositivos Moviles" ${val2}>Dispositivos Moviles</option>
                                        <option ${I} value="Inteligencia Artificial" ${val3}>Inteligencia Artifical</option>
                                        <option ${B} value="Base de Datos" ${val4}>Base de Datos</option>
                                        <option ${O} >Otra</option>
                                    </select>
                                </div>
                            </div>
                            <div class="containerLR percent divMultipleColumn">
                                <div class="class divMultipleRow">
                                    <div class="center divMultipleColumn percent overflowHidden">
                                        <div class="estado percent">
                                            <h3 class="tituloCuerpo2">Estado de desarrollo del proyecto</h3>
                                            <div class="idea">
                                                <input ${a} type="checkbox" name="estadoDesarrollo" value="Tengo la idea" id="idea" />
                                                <label for="idea">Tengo la idea</label>
                                            </div>
                                            <div class="disenio">
                                                <input ${o} type="checkbox" name="estadoDesarrollo" value="Tengo el diseño" id="disenio" />
                                                <label for="disenio">Tengo el dise�o</label>
                                            </div>
                                            <div class="especificaciones">
                                                <input ${s} type="checkbox" name="estadoDesarrollo" value="Tengo las especificaciones" id="especificaciones" />
                                                <label for="especificaciones">Tengo las especificaciones</label>
                                            </div>
                                            <div class="nada oculto">
                                                <input ${a} type="checkbox" name="estadoDesarrollo" value="nada" id="nada"/>
                                                <label for="nada">nada</label>
                                            </div>
                                        </div>
                                        <div class="api percent">
                                            <h3 class="tituloCuerpo2">Integraci�n con API</h3>
                                            <div class="socialM">
                                                <input ${So} type="checkbox" name="APIs" value="Social Media" id="socialM" />
                                                <label for="socialM">Social Media</label>
                                            </div>
                                            <div class="payment">
                                                <input ${Pa} type="checkbox" name="APIs" value="Payment" id="payment" />
                                                <label for="payment">Payment</label>
                                            </div>
                                            <div class="cloudS">
                                                <input ${Cl} type="checkbox" name="APIs" value="Cloud Storage" id="cloudS" />
                                                <label for="cloudS">Cloud Storage</label>
                                            </div>
                                            <div class="otros">
                                                <input ${Ot} type="checkbox" name="APIs" value="Otros" id="otros" />
                                                <label for="otros">Otros</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right divMultipleColumn percent overflowHidden">
                                        <div class="lenguaje percent">
                                            <h3 class="tituloCuerpo2">Lenguajes recomendados</h3>
                                            <input type="text" name="lenguaje" id="lenguaje" value="${lenguaje}"/>
                                        </div>
                                        <div class="entorno percent">
                                            <h3 class="tituloCuerpo2">Entrono Recomendado</h3>
                                            <input type="text" name="entorno" id="entorno" value="${entorno}"/>
                                        </div>
                                        <div class="entrega percent ">
                                            <h3 class="tituloCuerpo2">Plazo de entrega en d��as</h3>
                                            <input type="number" name="entregaDias" id="entregaDias" value="${entregaDias}"/>
                                        </div>
                                        <div class="adjuntar centrarHorizontal">
                                            <h3 class="tituloCuerpo2">Adjuntar archivos</h3>
                                            <input type="file" name="adjuntarArchivos" class="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="publica percent">
                                    <div class=" sCondiciones">
                                        <label for="terminos">
                                            <input type="checkbox" id="terminos" required/>
                                            He le��do y acepto los t�rminos y condiciones
                                        </label>
                                    </div>
                                    <button type="submit" value="${userId}" name="btnId" id="publicar" class="centrarHorizontal btnPublicar">Guardar cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form><br>
                <div id="${status}">
                    ${mensaje} <br>
                    ${variables}
                </div>
            </main>
        </div>
        <footer id="footer" class="foodter">footer</footer>
        <script src="JS/home.js"></script>
        <script src="JS/jquery-3.5.1.slim.min.js" type="text/javascript"></script>
        <script src="JS/popper.min.js" type="text/javascript"></script>
        <script src="JS/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>