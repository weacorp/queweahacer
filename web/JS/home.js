const contenedor = document.querySelector("#contenedor");

document.querySelector("#boton-menu").addEventListener("click", () => {
	contenedor.classList.toggle("active");
});

async function fetchHtmlAsText(url) {
	return await (await fetch(url)).text();
}

async function fetchHtmlAsText(url) {
	const response = await fetch(url);
	return await response.text();
}

// `load_Footer() function`
async function loadFooter() {
	const contentDiv = document.getElementById("footer");
	contentDiv.innerHTML = await fetchHtmlAsText("footer.html");
}

// `load_Header() function`
//async function loadHeader() {
//	const contentDiv = document.getElementById("header");
//	contentDiv.innerHTML = await fetchHtmlAsText("head.jsp");
//}

// `load_Header() function`
//async function loadMenuLateral() {
//	const contentDiv = document.getElementById("menuLateraltItems");
//	contentDiv.innerHTML = await fetchHtmlAsText("menuLateral.html");
//}

// `load_catalogoProyectos() function`
// async function loadCatalogoProyectos() {
// 	const contentDiv = document.getElementById("content");
// 	contentDiv.innerHTML = await fetchHtmlAsText("crearPublicacion.jsp");
// }

loadFooter();
loadHeader();
loadMenuLateral();
// loadCatalogoProyectos();