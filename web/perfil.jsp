<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/signup.css">
        <link rel="stylesheet" type="text/css" href="css/head.css" />
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <link rel="stylesheet" type="text/css" href="css/armaTarjeta.css" />
        <link rel="stylesheet" type="text/css" href="css/menu.css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <title>SignUp</title>
    </head>

    <body>
        <%
            if (session.getAttribute("login") == null) {
                response.sendRedirect("login.jsp");
            }
        %>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>
                </div>
                <div id="linksHead" class="percent centradoAbsoluto">
                    ${head}
                </div>
            </div>
        </div>
        <div id="contenedor" class="contenedor centrarHorizontal">
            <nav id="menu-lateral" class="menu-lateral">
                <button id="boton-menu"><img id="menu-icon" src="images/menuw.png" /></button>
                <nav class="menu-lateral">
                    <a href="CrearPublicacion">Crear publicaci�n</a>
                    <a href="MisPublicaciones">Mis publicaciones</a>
                    <a href="ListaFavoritos">Favoritos</a>
                </nav>
            </nav>
            <main id="content" class="main">
                <h3 class="tituloMain">Perfil</h3>
                <form class="signup-form" action="UpdateUser" method="POST">
                    <div class="form-container">
                        <div class="data-container">
                            <div class="form-first">
                                <label for="name">Nombre(s):</label><br>
                                <input type="text" name="name" placeholder="Juan" required value="${name}"><br>
                                <label for="lastname">Apellidos</label><br>
                                <input type="text" name="lastname" placeholder="L�pez..." required value="${lastname}"><br>
                                <label for="email">Correo electr�nico</label><br>
                                <input type="email" name="email" placeholder="mail@mail.com" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${email}"><br>
                                <label for="email-confirm">Confirmar correo</label><br>
                                <input type="email" name="email-confirm" placeholder="Repita el email" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${emailC}"><br>
                            </div>
                            <div class="form-second">
                                <label for="tel">Tel�fono</label><br>
                                <input type="number" name="tel" placeholder="10 digitos" required max="9999999999" value="${tel}"><br>
                                <label for="tel-confirm">Confirmar tel�fono</label><br>
                                <input type="number" name="tel-confirm" placeholder="Repita el tel�fono" required max="9999999999" value="${telC}"><br>
                                <label for="password">Contrase�a</label><br>
                                <input type="password" name="password" placeholder="Ingrese su contrase�a" required value="${pass}"><br>
                                <label for="password-confirm">Confirmar contrase�a</label><br>
                                <input type="password" name="password-confirm" placeholder="Repita su contrase�a" required value="${passC}"><br>
                            </div>
                        </div>
                        <div class="buttons-row">
                            <div class="form-confirm">
                                <button type="submit" value="UpdateUser">Guardar cambios</button><br>
                                <div id="${status}">
                                    ${mensaje}
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </main>

        </div>
        <footer id="footer" class="foodter">footer</footer>
        <script src="JS/home.js"></script>
        <script src="JS/jquery-3.5.1.slim.min.js" type="text/javascript"></script>
        <script src="JS/popper.min.js" type="text/javascript"></script>
        <script src="JS/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>