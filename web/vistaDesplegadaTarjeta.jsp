<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="css/head.css" />
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <link rel="stylesheet" type="text/css" href="css/vistaDesplegadaTarjeta.css" />
        <link rel="stylesheet" type="text/css" href="css/armaTarjeta.css" />
        <link rel="stylesheet" type="text/css" href="css/menu.css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Formato Lista</title>
    </head>
    <body>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>
                </div>
                <div id="linksHead" class="percent centradoAbsoluto">
                    ${head}
                </div>
            </div>
        </div>
        <div id="contenedor" class="contenedor centrarHorizontal">
            <nav id="menu-lateral" class="menu-lateral">
                <button id="boton-menu"><img id="menu-icon" src="images/menuw.png" /></button>
                <nav class="menu-lateral">
                    <a href="CrearPublicacion">Crear publicaci�n</a>
                    <a href="MisPublicaciones">Mis publicaciones</a>
                    <a href="ListaFavoritos">Favoritos</a>
                </nav>

            </nav>
            <main id="content" class="main">
                <div class="cuerpoBase cuerpoTarjeta backgroundColor textColor">
                    <div class="divMultipleColumn overflowHidden tarjeta">
                        <div class="titulo overflow scroller">
                            <h2>${nomProyecto}</h2>
                        </div>
                        <div class=" contentBo divMultipleRow">
                            <div class="altura divMultipleColumn  cuerpoVIzquierda">
                                <div class="vDescripcion overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Descripci�n del proyecto</h3>
                                    <div class="overflow scroller">
                                        <p id="vTextDescipcion" class="textoCuerpo">${brDescripcion}</p>
                                    </div>
                                </div>
                                <div class="vCategoria overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Categor��a</h3>
                                    <div class="overflow scroller ">
                                        <p id="vTextCategoria" class="textoCuerpo">
                                            ${categoria}
                                        </p>
                                    </div>
                                </div>
                                <div class="vLenguajeR overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Lenguajes recomendados</h3>
                                    <div class="overflow scroller">
                                        <p id="vTextoLenguaje" class="textoCuerpo">${lenguajeR}</p>
                                    </div>
                                </div>
                                <div class="vEntornoR overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Entorno recomendado</h3>
                                    <div class="overflow scroller">
                                        <p id="vTextoEntorno" class="textoCuerpo">${entornoR}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="altura cuerpoVCentral divMultipleColumn">
                                <div class="vEstDesarrollo overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Estado de desarrollo</h3>
                                    <div class="overflow scroller">
                                        <p id="vEstDesarrollo" class="textoCuerpo">${estDesarrollo}</p>
                                    </div>
                                </div>
                                <div class="vIntApi overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Integraci�n con una API</h3>
                                    <div class="overflow scroller">
                                        <p id="vIntApi" class="textoCuerpo">${intAPI}</p>
                                    </div>
                                </div>
                                <div class="vEntrega overflowHidden divMultipleColumn percent">
                                    <h3 class="tituloCuerpo">Plazo de entrega (d��as)</h3>
                                    <div class="overflow scroller">
                                        <p id="vEntrega" class="textoCuerpo">${plzDias}</p>
                                    </div>
                                </div>
                                <div class="vAutorIdea overflowHidden divMultipleColumn percent ">
                                    <h3 class="tituloCuerpo">Autor de la idea</h3>
                                    <div class="overflow scroller">
                                        <p id="vAutorIdea" class="textoCuerpo">${autorIdea}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="altura cuerpoVDerecha divMultipleColumn ">
                                <div class="contenedorDocumento centradoAbsoluto percent ">
                                    <div class="Documentos percent centradoAbsoluto">
                                        <img class="" src="images/documentos.png" alt="pdf">
                                    </div>
                                </div>
                                <div class="percent centradoAbsoluto">
                                    <div class="contacto">
                                        <a class="noDecoration" href="InformacionDeContacto?btnId=${btnId}">
                                            <img class="centrarHorizontal" src="images/contacto.png" alt="" />
                                            <h3>Contactar</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>
                <div id="${status}">
                    ${mensaje}
                </div>
                <!-- div.vDescripcion>h3.tituloCuerpo+div.overflow.scroller>p.vTextDescipcion+div.vCategoria+.vLenguajeR+.vEntornoR -->
                <!-- div.v>h3.tituloCuerpo{}+div.overflow.scroller>p#v.textoCuerpo{} -->
                <!-- div.Documentos+div.vContacto>a[href="#"]>img[src="images/contacto.png"]+h3{Contactar} -->
            </main>
        </div>
        <footer id="footer" class="foodter">footer</footer>
        <script src="JS/home.js"></script>
        <script src="JS/jquery-3.5.1.slim.min.js" type="text/javascript"></script>
        <script src="JS/popper.min.js" type="text/javascript"></script>
        <script src="JS/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>
