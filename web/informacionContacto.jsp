<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" type="text/css" href="css/head.css" />
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <link rel="stylesheet" href="css/signup.css">
        <link rel="stylesheet" type="text/css" href="css/menu.css" />
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>Informaci�n de Contacto</title>
    </head>
    <%
        if (session.getAttribute("login") == null) {
            response.sendRedirect("login.jsp");
        }
    %>
    <body>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>
                </div>
                <div id="linksHead" class="percent centradoAbsoluto">
                    ${head}
                </div>
            </div>
        </div>
        <div id="contenedor" class="contenedor centrarHorizontal">
            <nav id="menu-lateral" class="menu-lateral">
                <button id="boton-menu"><img id="menu-icon" src="images/menuw.png" /></button>
                <nav class="menu-lateral">
                    <a href="CrearPublicacion">Crear publicaci�n</a>
                    <a href="MisPublicaciones">Mis publicaciones</a>
                    <a href="ListaFavoritos">Favoritos</a>
                </nav>
            </nav>
            <main id="content" class="main">
                <h3 class="tituloMain">Informaci�n de Contacto</h3>
                <form class="signup-form" action="SendEmail" method="POST">
                    <div class="signup-form">
                        <div class="data-container">
                            <div class="form-first">
                                <label for="name">Nombre(s):</label><br>
                                <input type="text" name="name" placeholder="Juan" required value="${name}"><br>
                                <label for="email">Correo electr�nico</label><br>
                                <input type="email" name="email" placeholder="mail@mail.com" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" value="${email}"><br>
                                <label for="tel">Tel�fono</label><br>
                                <input type="number" name="tel" placeholder="10 digitos" required max="9999999999" value="${tel}"><br>
                                <label for="Skype">Skype</label><br>
                                <input type="text" name="skype" required value="${skype}"><br>
                            </div>
                            <div class="form-second">
                                <label for="Website">GitHub</label><br>
                                <input type="text" name="github" required value="${github}"><br>
                                <label for="linkenid">Linkenid</label><br>
                                <input type="text" name="linkenid" required value="${linkenid}"><br>
                                <label for="Message">Escribe un mensaje</label><br>
                                <textarea name="message" class="textareac" required>${message}</textarea><br>
                            </div>
                        </div>
                    </div>
                    <div class="buttons-row">
                        <div class="form-confirm">
                                <button type="submit" name="btnId" value="${publicacionId}">Contactar</button><br>
                        </div>
                    </div>
                </form>
                <div id="${status}">
                    ${mensaje}
                </div>
            </main>
        </div>
        <footer id="footer" class="foodter">footer</footer>
        <script src="JS/home.js"></script>
        <script src="JS/jquery-3.5.1.slim.min.js" type="text/javascript"></script>
        <script src="JS/popper.min.js" type="text/javascript"></script>
        <script src="JS/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>