<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/login.css">
        <link rel="stylesheet" type="text/css" href="css/head.css" />
        <link rel="stylesheet" type="text/css" href="css/homeGrid.css" />
        <title>LogIn</title>
    </head>

    <body>
        <div id="contentAllHead" class="centrarHorizontal centradoAbsoluto percent">
            <div id="contentHead" class="divMultipleRow centrarVertical percent">
                <div id="logoHead">
                    <a href="index.jsp">
                        <img src="images/logo.png"/>
                    </a>                    
                </div>
            </div>
        </div>
        <div id="login">
            <div id="form-title">
                <h1 class="title">Iniciar sesi�n</h1>
            </div>
            <form id="login-form" action="LogIn" method="POST">
                <label for="email">Correo electr�nico</label><br>
                <input type="text" name="email" placeholder="Ingrese su correo" required><br>
                <label for="password">Contrase�a</label><br>
                <input type="password" name="password" placeholder="Ingrese su contrase�a" required><br>
                <button type="submit" value="LogIn">Ingresar</button><br>
                <a href="signup.jsp">�A�n no tienes cuenta?<br>
                    Registrate aqu��</a>
            </form><br>
            <div id="${status}">
                ${mensaje}
            </div>
        </div>
        <%
            if (session.getAttribute("login") != null) {
                response.sendRedirect("index.jsp");
            }
        %>
        <footer id="footer" class="foodter">footer</footer>
        <script src="JS/home.js"></script>
    </body>

</html>