package DAO;

import Model.Favorito;
import java.sql.SQLException;
import java.util.Collection;

public interface FavoritoDAO {
    public void conectar() throws SQLException;
    public void desconectar() throws SQLException; 
    public boolean validarFavorito(Favorito f) throws SQLException;
    public Favorito buscar(int id) throws SQLException;
    public Collection<Favorito> buscar(String condicion) throws SQLException;
    public int agregar(Favorito e) throws SQLException;
    public int eliminar(int id) throws SQLException;
    public int modificar(Favorito e) throws SQLException;
}