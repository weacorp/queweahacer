package DAO;

import Model.Usuario;
import java.sql.SQLException;
import java.util.Collection;

public interface UsuarioDAO {
    
    public void conectar() throws SQLException;
     public Usuario autorizarLogIn (Usuario u) throws SQLException; 
    public void desconectar() throws SQLException;    
    public Usuario buscar(int id) throws SQLException;
    public Collection<Usuario> buscar(String condicion) throws SQLException;
    public int agregar(Usuario e) throws SQLException;
    public int eliminar(int id) throws SQLException;
    public int modificar(Usuario u) throws SQLException;
    public boolean existeUsuario(Usuario u, String cond) throws SQLException;
 
}
