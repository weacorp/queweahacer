package DAO;

import Model.Publicacion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class PublicacionDAOJavaBD implements PublicacionDAO {

    private Connection conexion;
//    private final String URL = "jdbc:derby://localhost:1527/queWEAhacer";
//    private final String usuario = "root";
//    private final String contrasena = "root";

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public Connection getConexion() {
        return conexion;
    }

    //+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
    @Override
    public void conectar() throws SQLException {
        //conexion = null;
        //conexion = DriverManager.getConnection(URL, usuario, contrasena);
        String nombrebd = "sql9346374";
        String user = "sql9346374";
        String pass = "JACvbcci5s";
        String url = "jdbc:mysql://sql9.freesqldatabase.com:3306/" + nombrebd;
//String nombrebd = "queWEAhacer";
//        String user = "root";
//        String pass = "";
//        String url = "jdbc:mysql://localhost:3306/"+nombrebd;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(url, user, pass);
            if (conexion != null) {
                System.out.println("Conexion exitosa");
            }

        } catch (ClassNotFoundException cnfe) {
            System.out.println("No encuentra la clase " + cnfe);
        } catch (SQLException sqle) {
            System.err.println("SQLException " + sqle);
        }
    }

    @Override
    public void desconectar() throws SQLException {
        conexion.close();
    }

    @Override
    public Publicacion buscar(int id) throws SQLException {
        ResultSet rs;
        Statement st;
        Publicacion p = null;
        String sql = "SELECT * FROM PUBLICACION WHERE id = " + id;

        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            p = new Publicacion();
            p.setId(rs.getInt("id"));
            p.setCategoria(rs.getString("categoria"));
            p.setNomProyecto(rs.getString("nomProyecto"));
            p.setEstDesarrollo(p.splitListaChecks(rs.getString("estDesarrollo")));
            p.setBrDescripcion(rs.getString("brDescripcion"));
            p.setLenguajeR(rs.getString("lenguajeR"));
            p.setEntornoR(rs.getString("entornoR"));
            p.setIntApi(p.splitListaChecks(rs.getString("intApi")));
            p.setIdUsuario(rs.getInt("idUsuario"));
            p.setPlzDias(rs.getInt("plzDias"));
            p.setAdjArchivos(rs.getString("adjArchivos"));
        }
        return p;
    }

    @Override
    public Collection<Publicacion> buscar(String condicion) throws SQLException {
        ResultSet rs;
        Statement st;
        Publicacion p;
        String sql = "SELECT * FROM PUBLICACION " + condicion;
        ArrayList<Publicacion> list = new ArrayList<>();
        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            p = new Publicacion();
            p.setId(rs.getInt("id"));
            p.setCategoria(rs.getString("categoria"));
            p.setNomProyecto(rs.getString("nomProyecto"));
            p.setEstDesarrollo(p.splitListaChecks(rs.getString("estDesarrollo")));
            p.setBrDescripcion(rs.getString("brDescripcion"));
            p.setLenguajeR(rs.getString("lenguajeR"));
            p.setEntornoR(rs.getString("entornoR"));
            p.setIntApi(p.splitListaChecks(rs.getString("intApi")));
            p.setPlzDias(rs.getInt("plzDias"));
            p.setIdUsuario(rs.getInt("idUsuario"));
            p.setAdjArchivos(rs.getString("adjArchivos"));
            list.add(p);
        }
        return list;
    }

    @Override
    public int agregar(Publicacion p) throws SQLException {
        String sql;
        Statement st;
        int resultado;

        sql = " INSERT INTO PUBLICACION (categoria, nomProyecto, estDesarrollo, brDescripcion, lenguajeR, entornoR, intApi, plzDias, adjArchivos, idUsuario) "
                + " VALUES ( '"
                + p.getCategoria() + "'"
                + ", '" + p.getNomProyecto() + "'"
                + ", '" + p.armaStringListaChecks(p.getEstDesarrollo()) + "'"
                + ", '" + p.getBrDescripcion() + "'"
                + ", '" + p.getLenguajeR() + "'"
                + ", '" + p.getEntornoR() + "'"
                + ", '" + p.armaStringListaChecks(p.getIntApi()) + "'"
                + ", " + p.getPlzDias()
                + ", '" + p.getAdjArchivos() + "'"
                + ", " + p.getIdUsuario() + " )";

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

    @Override
    public int eliminar(int id) throws SQLException {
        Statement st;
        String sql;
        int resultado;
        sql = "DELETE FROM PUBLICACION WHERE ID =" + id;

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

    @Override
    public int modificar(Publicacion p) throws SQLException {
        String sql;
        Statement st;
        int resultado;

        sql = " UPDATE PUBLICACION SET "
                + "CATEGORIA = '" + p.getCategoria() + "',"
                + "NOMPROYECTO = '" + p.getNomProyecto() + "',"
                + "ESTDESARROLLO = '" + p.armaStringListaChecks(p.getEstDesarrollo()) + "',"
                + "BRDESCRIPCION = '" + p.getBrDescripcion() + "',"
                + "LENGUAJER = '" + p.getLenguajeR() + "',"
                + "ENTORNOR = '" + p.getEntornoR() + "',"
                + "INTAPI = '" + p.armaStringListaChecks(p.getIntApi()) + "',"
                + "PLZDIAS = " + p.getPlzDias() + ","
                + "IDUSUARIO = " + p.getIdUsuario() + " "
                + "WHERE ID = " + p.getId();

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }
}
