package DAO;

import Model.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class UsuarioDAOJavaDB implements UsuarioDAO {

    private Connection conexion;
//    private final String URL = "jdbc:derby://localhost:1527/queWEAhacer";
//    private final String usuario = "root";
//    private final String contrasena = "root";

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public Connection getConexion() {
        return conexion;
    }
    //+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
    @Override
    public void conectar() throws SQLException {
        //conexion = null;
        //conexion = DriverManager.getConnection(URL, usuario, contrasena);
        String nombrebd = "sql9346374";
        String user = "sql9346374";
        String pass = "JACvbcci5s";
        String url = "jdbc:mysql://sql9.freesqldatabase.com:3306/"+nombrebd;
//String nombrebd = "queWEAhacer";
//        String user = "root";
//        String pass = "";
//        String url = "jdbc:mysql://localhost:3306/"+nombrebd;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(url,user,pass);
            if (conexion!=null) {
                System.out.println("Conexion exitosa");
            }
            
        } catch (ClassNotFoundException cnfe) {
            System.out.println("No encuentra la clase " + cnfe);
        } catch (SQLException sqle) {
            System.err.println("SQLException "+sqle);
        }
    }

    @Override
    public void desconectar() throws SQLException {
        conexion.close();
    }
    
    @Override
    public Usuario autorizarLogIn (Usuario u) throws SQLException {
        String email = u.getEmail();
        String pass = u.getContrasenia();
        
        String DBEmail = "" , DBPass = "";
        Statement st = null;
        ResultSet rs;
        String sql = "SELECT * FROM USUARIO WHERE EMAIL = '"
                +email+"' AND CONTRASENIA = '"+pass+"'";
        st = conexion.createStatement();
        rs = st.executeQuery(sql);
        
        while(rs.next()){
            DBEmail = rs.getString("email");
            DBPass = rs.getString("contrasenia");
            
            if (email.equals(DBEmail) && pass.equals(DBPass)) {
                u.setId(rs.getInt("id"));
                u.setNombres(rs.getString("nombres"));
                u.setApellidos(rs.getString("apellidos"));
                return u;
            }
        }
        st.close();
        conexion.close();
        return u;
    }

    @Override
    public Usuario buscar(int id) throws SQLException {
        ResultSet rs;
        Statement st;
        Usuario e = null;
        String sql = "SELECT * FROM USUARIO WHERE id = " + id;

        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            e = new Usuario();
            e.setId(rs.getInt("id"));
            e.setNombres(rs.getString("nombres"));
            e.setApellidos(rs.getString("apellidos"));
            e.setEmail(rs.getString("email"));
            e.setTelefono(rs.getString("telefono"));
            e.setContrasenia(rs.getString("contrasenia"));
        }
        return e;
    }

    @Override
    public Collection<Usuario> buscar(String condicion) throws SQLException {
        ResultSet rs;
        Statement st;
        Usuario u;
        String sql = "SELECT * FROM USUARIO "+condicion;
        ArrayList<Usuario> list = new ArrayList<Usuario>();
        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            u = new Usuario();
            u.setId(rs.getInt("id"));
            u.setNombres(rs.getString("nombres"));
            u.setApellidos(rs.getString("apellidos"));
            u.setEmail(rs.getString("email"));
            u.setTelefono(rs.getString("telefono"));
            u.setContrasenia(rs.getString("contrasenia"));
            list.add(u);
        }
        return list;
    }

    @Override
    public int agregar(Usuario u) throws SQLException {
        String sql;
        Statement st;
        int resultado;

        sql = " INSERT INTO USUARIO (NOMBRES, APELLIDOS, EMAIL, TELEFONO, CONTRASENIA) "
                + " VALUES ( '"
                + u.getNombres() + "'"
                + ", '" + u.getApellidos() + "'"
                + ", '" + u.getEmail() + "'"
                + ", '" + u.getTelefono() + "'"
                + ", '" + u.getContrasenia() + "' )";

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

    @Override
    public int eliminar(int id) throws SQLException {
        Statement st;
        String sql;
        int resultado;
        sql = "DELETE FROM USUARIO WHERE ID =" + id;

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

    @Override
    public int modificar(Usuario u) throws SQLException {
        String sql;
        Statement st;
        int resultado;

        sql = " UPDATE USUARIO SET "
                + "NOMBRES = '" + u.getNombres() + "',"
                + "APELLIDOS = '" + u.getApellidos() + "',"
                + "EMAIL = '" + u.getEmail() + "',"
                + "TELEFONO = '" + u.getTelefono() +"',"
                + "CONTRASENIA = '" + u.getContrasenia() +"' "
                + "WHERE ID = " + u.getId();

        System.out.println(sql);
        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }
    
    @Override
    public boolean existeUsuario(Usuario u, String cond) throws SQLException {
        String sql;
        Statement st;
        ResultSet rs;
        
        sql = "SELECT * FROM USUARIO " + cond;
        System.out.println(sql);
        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        return rs.next();
    }

}
