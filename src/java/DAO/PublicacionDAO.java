package DAO;

import Model.Publicacion;
import java.sql.SQLException;
import java.util.Collection;

public interface PublicacionDAO {
    public void conectar() throws SQLException;
    public void desconectar() throws SQLException;    
    public Publicacion buscar(int id) throws SQLException;
    public Collection<Publicacion> buscar(String condicion) throws SQLException;
    public int agregar(Publicacion e) throws SQLException;
    public int eliminar(int id) throws SQLException;
    public int modificar(Publicacion e) throws SQLException;
}