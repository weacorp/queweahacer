package DAO;

import Model.Favorito;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

public class FavoritoDAOJavaDB implements FavoritoDAO {

    private Connection conexion;
    //private final String URL = "jdbc:derby://localhost:1527/queWEAhacer";
    //private final String usuario = "root";
    //private final String contrasena = "root";

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public Connection getConexion() {
        return conexion;
    }

    //+"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
    @Override
    public void conectar() throws SQLException {
        //conexion = null;
        //conexion = DriverManager.getConnection(URL, usuario, contrasena);
        String nombrebd = "sql9346374";
        String user = "sql9346374";
        String pass = "JACvbcci5s";
        String url = "jdbc:mysql://sql9.freesqldatabase.com:3306/" + nombrebd;
//        String nombrebd = "queWEAhacer";
//        String user = "root";
//        String pass = "";
//        String url = "jdbc:mysql://localhost:3306/"+nombrebd;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(url, user, pass);
            if (conexion != null) {
                System.out.println("Conexion exitosa");
            }

        } catch (ClassNotFoundException cnfe) {
            System.out.println("No encuentra la clase " + cnfe);
        } catch (SQLException sqle) {
            System.err.println("SQLException " + sqle);
        }
    }

    @Override
    public void desconectar() throws SQLException {
        conexion.close();
    }

    @Override
    public boolean validarFavorito(Favorito f) throws SQLException {
        int user = f.getAutorId();
        int pub = f.getPublicacionId();

        int DBUser, DBPub;
        Statement st = null;
        ResultSet rs;
        String sql = "SELECT * FROM FAVORITOS WHERE USUARIOID = "
                + user + " AND PUBLICACIONID = " + pub + "";
        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            DBUser = rs.getInt("usuarioid");
            DBPub = rs.getInt("publicacionid");

            if ((user == DBUser) && (pub == DBPub)) {
                return true;
            }
        }
        st.close();
//        conexion.close();
        return false;
    }

    @Override
    public Favorito buscar(int id) throws SQLException {
        ResultSet rs;
        Statement st;
        Favorito f = null;
        String sql = "SELECT * FROM FAVORITOS WHERE id = " + id;

        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            f = new Favorito();
            f.setFavoritoId(rs.getInt("ID"));
            f.setAutorId(rs.getInt("USUARIOID"));
            f.setPublicacionId(rs.getInt("PUBLICACIONID"));
        }
        return f;
    }

    @Override
    public Collection<Favorito> buscar(String condicion) throws SQLException {
        ResultSet rs;
        Statement st;
        Favorito f;
        String sql = "SELECT * FROM FAVORITOS " + condicion;
        ArrayList<Favorito> list = new ArrayList<>();
        st = conexion.createStatement();
        rs = st.executeQuery(sql);

        while (rs.next()) {
            f = new Favorito();
            f.setFavoritoId(rs.getInt("ID"));
            f.setAutorId(rs.getInt("USUARIOID"));
            f.setPublicacionId(rs.getInt("PUBLICACIONID"));
            list.add(f);
        }
        return list;
    }

    @Override
    public int agregar(Favorito f) throws SQLException {
        String sql;
        Statement st;
        int resultado;

        sql = " INSERT INTO FAVORITOS (USUARIOID, PUBLICACIONID) "
                + " VALUES ( "
                + f.getAutorId()
                + ", " + f.getPublicacionId() + " )";

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

    @Override
    public int eliminar(int id) throws SQLException {
        Statement st;
        String sql;
        int resultado;
        sql = "DELETE FROM FAVORITOS WHERE ID =" + id;

        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

    @Override
    public int modificar(Favorito f) throws SQLException {
        String sql;
        Statement st;
        int resultado;

        sql = " UPDATE FAVORITOS SET "
                + "USUARIOID = " + f.getAutorId() + ","
                + "PUBLICACIONID = " + f.getPublicacionId()
                + "WHERE ID = " + f.getFavoritoId();
        System.out.println(sql);
        st = conexion.createStatement();
        resultado = st.executeUpdate(sql);
        return resultado;
    }

}
