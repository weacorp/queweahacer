package Test;

import DAO.UsuarioDAOJavaDB;
import java.sql.SQLException;

public class TestConnect {

    public static void main(String[] args) {

        UsuarioDAOJavaDB edb = new UsuarioDAOJavaDB();

        try {
            edb.conectar();
            edb.desconectar();
        } catch (SQLException ex) {
            System.out.println("No se pudo abrir la base de datos, causa:");
            System.out.println(ex.getMessage());
            System.exit(1);
        }

        System.out.println("La BD de abrió y cerró exitosamente !!!!!!");
    }
}

