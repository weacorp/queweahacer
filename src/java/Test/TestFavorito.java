package Test;

import DAO.PublicacionDAOJavaBD;
import java.sql.SQLException;
import Model.Usuario;
import DAO.UsuarioDAOJavaDB;
import Model.Publicacion;

public class TestFavorito {

    public static void main(String[] args) {
        int resultado;
        Publicacion e = new Publicacion();
        String arrayCaracteres[];
        arrayCaracteres = new String[10];
        arrayCaracteres[0] = "Tengo la idea";

//        e.setId(3);
        e.setAdjArchivos("");
        e.setBrDescripcion("Descripcion");
        e.setCategoria("Base de Datos");
        e.setEntornoR("NetBeans");
        e.setEstDesarrollo(arrayCaracteres);
        e.setIdUsuario(1);
        e.setIntApi(arrayCaracteres);
        e.setLenguajeR("Java");
        e.setNomProyecto("Hola mundo");
        e.setPlzDias(1);

        // Insertar en la BD
        PublicacionDAOJavaBD edb = new PublicacionDAOJavaBD();

        try {
            edb.conectar();
            resultado = edb.agregar(e);
            if (resultado == 0) {
                System.out.println("No se pudo agregar");
            } else {
                System.out.println("Usuario agregado exitosamente");
            }

            edb.desconectar();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
//        try {
//            boolean var;
//            edb.conectar();
//            var = edb.existeUsuario(e, "WHERE email = " + e.getEmail());
//            if (var == false) {
//                System.out.println("El usuario no existe");
//            } else {
//                System.out.println("El usuario ya existe");
//            }
//
//            edb.desconectar();
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
    }
}
