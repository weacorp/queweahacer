package Test;

import DAO.UsuarioDAOJavaDB;
import Model.Usuario;
import java.sql.SQLException;

public class TestUpdate {
    public static void main(String[] args) {
        int resultado;
        Usuario e = new Usuario();

        e.setId(1);
        e.setNombres("Juanito");
        e.setApellidos("Ramirez");
        e.setEmail("nuevomail@mail.com");
        e.setTelefono("4772873986");
        e.setContrasenia("234");

        // Actualizar en la BD
        UsuarioDAOJavaDB uDB = new UsuarioDAOJavaDB();

        try {
            uDB.conectar();
            resultado = uDB.modificar(e);
            if (resultado == 0) {
                System.out.println("No se pudo actualizar");
            } else {
                System.out.println("Usuario actualizado exitosamente");
            }

            uDB.desconectar();
        } catch (SQLException ex) { 
            System.out.println(ex.getMessage());
        }

    }
}
