package Test;

import java.sql.SQLException;
import Model.Usuario;
import DAO.UsuarioDAOJavaDB;

public class TestSignUp {

    public static void main(String[] args) {
        int resultado;
        Usuario e = new Usuario();

//        e.setId(3);
        e.setNombres("Daniel");
        e.setApellidos("Bueno");
        e.setEmail("mail2@mail.com");
        e.setTelefono("4777877898");
        e.setContrasenia("123");

        // Insertar en la BD
        UsuarioDAOJavaDB edb = new UsuarioDAOJavaDB();

        try {
            edb.conectar();
            resultado = edb.agregar(e);
            if (resultado == 0) {
                System.out.println("No se pudo agregar");
            } else {
                System.out.println("Usuario agregado exitosamente");
            }

            edb.desconectar();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        try {
            boolean var;
            edb.conectar();
            var = edb.existeUsuario(e, "WHERE email = " + e.getEmail());
            if (var == false) {
                System.out.println("El usuario no existe");
            } else {
                System.out.println("El usuario ya existe");
            }

            edb.desconectar();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
