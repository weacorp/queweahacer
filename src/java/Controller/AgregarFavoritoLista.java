package Controller;

import Model.Favorito;
import DAO.FavoritoDAOJavaDB;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "AgregarFavoritoLista", urlPatterns = {"/AgregarFavoritoLista"})
public class AgregarFavoritoLista extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "ListaProyectos";

        int res = 0;
        Favorito f = new Favorito();
        FavoritoDAOJavaDB fDB = new FavoritoDAOJavaDB();
        HttpSession session = request.getSession();
        EnviarHead h = new EnviarHead();
        String head;

        if (session.getAttribute("login") != null) {
            int idUser = (int) session.getAttribute("login");
            head = h.pedirHead(true, session);
            f.setAutorId(idUser);
            f.setPublicacionId(Integer.parseInt(request.getParameter("btnId")));

            fDB.conectar();
            if (fDB.validarFavorito(f) == true) {
                request.setAttribute("status", "error");
                request.setAttribute("mensaje", "Ya se encuentra en tu lista de favoritos");
            } else {
                res = fDB.agregar(f);
                fDB.desconectar();
                if (res == 0) {
                    request.setAttribute("status", "error");
                    request.setAttribute("mensaje", "Error al conectar con el servidor");
                } else {
                    request.setAttribute("status", "correcto");
                    request.setAttribute("mensaje", "Agregado con exito a la lista de Favoritos");
                }
            }
        } else {
            paginaRespuesta = "login.jsp";
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);

        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
