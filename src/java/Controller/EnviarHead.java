package Controller;

import javax.servlet.http.HttpSession;

public class EnviarHead {

    public String pedirHead(boolean s, HttpSession session) {
        String head = "";
        
        if (s) {
            head = "    <div class=\"dropdown mt-3\">\n"
                    + "     <button style=\"border: none\" class=\"btn btn-outline-light btn-lg dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n"
                    + "         " + session.getAttribute("user")
                    + "     </button>\n"
                    + "     <div class=\"dropdown-menu text-center\">\n"
                    + "         <a class=\"dropdown-item\" href=\"Perfil\">\n"
                    + "             <img src=\"images/perfil.png\" width=\"60\" alt=\"60\"/>\n"
                    + "         </a>\n"
                    + "         <a class=\"dropdown-item\" href=\"Perfil\">" + session.getAttribute("user") + "</a>\n"
                    + "         <a class=\"dropdown-item\" href=\"Perfil\">" + session.getAttribute("email") + "</a>\n"
                    + "         <div class=\"dropdown-divider\"></div>\n"
                    + "         <a class=\"dropdown-item\" href=\"logout.jsp\">Cerrar sesi�n</a>\n"
                    + "     </div>\n"
                    + " </div>";
        } else {
            head = "<a href=\"login.jsp\" class=\"btnHead\">Iniciar sesi�n</a>\n"
                    + "<a href=\"signup.jsp\" class=\"btnHead\">Reg�strate</a>";
        }
        return head;
    }

}
