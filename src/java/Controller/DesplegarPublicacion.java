package Controller;

import DAO.PublicacionDAOJavaBD;
import DAO.UsuarioDAOJavaDB;
import Model.Publicacion;
import Model.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "DesplegarPublicacion", urlPatterns = {"/DesplegarPublicacion"})
public class DesplegarPublicacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "vistaDesplegadaTarjeta.jsp";
        
        String mensaje;
        Publicacion p = new Publicacion();
        Usuario u;
        PublicacionDAOJavaBD pDB = new PublicacionDAOJavaBD();
        UsuarioDAOJavaDB uDB = new UsuarioDAOJavaDB();
        HttpSession session = request.getSession();
        EnviarHead h = new EnviarHead();
        String head;
        
        if (session.getAttribute("login") != null) {
            head = h.pedirHead(true, session);
        } else {
            head = h.pedirHead(false, session);
        }

        p.setId(Integer.parseInt(request.getParameter("btnId")));

        try {
            pDB.conectar();
            p = pDB.buscar(p.getId());
            pDB.desconectar();
            uDB.conectar();
            u = uDB.buscar(p.getIdUsuario());
            uDB.desconectar();
            mensaje = "Vista deslpegada con exito";
            request.setAttribute("status", "correcto");
            request.setAttribute("mensaje", mensaje);
            request.setAttribute("nomProyecto", p.getNomProyecto());
            request.setAttribute("categoria", p.getCategoria());
            request.setAttribute("brDescripcion", p.getBrDescripcion());
            request.setAttribute("estDesarrollo", p.armaStringListaChecks(p.getEstDesarrollo()));
            request.setAttribute("entornoR", p.getEntornoR());
            request.setAttribute("lenguajeR", p.getLenguajeR());
            request.setAttribute("intAPI", p.armaStringListaChecks(p.getIntApi()));
            request.setAttribute("plzDias", p.getPlzDias());
            request.setAttribute("autorIdea", u.getNombres()+" "+u.getApellidos());
            request.setAttribute("adjArchivos", p.getAdjArchivos());
            request.setAttribute("btnId", p.getId()); 
        } catch (SQLException ex) {
            mensaje = "Servicio no disponible";
            request.setAttribute("status", "error");
            request.setAttribute("mensaje", mensaje);
        }
        request.setAttribute("head", head);


        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
        
    } 
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DesplegarPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DesplegarPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}