package Controller;

import DAO.PublicacionDAOJavaBD;
import DAO.UsuarioDAOJavaDB;
import Model.InfoContacto;
import Model.Publicacion;
import Model.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "InformacionDeContacto", urlPatterns = {"/InformacionDeContacto"})
public class InformacionDeContacto extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
    
        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "informacionContacto.jsp";
        
        Usuario u;
        UsuarioDAOJavaDB uBD = new UsuarioDAOJavaDB();
        Publicacion p = new Publicacion();
        PublicacionDAOJavaBD pDB = new PublicacionDAOJavaBD();
        
        HttpSession session = request.getSession();
        String head;
        EnviarHead h = new EnviarHead();
        if (session.getAttribute("login") != null) {
            head = h.pedirHead(true, session);
        
        
        p.setId(Integer.parseInt(request.getParameter("btnId")));
        
        try {
            pDB.conectar();
            p = pDB.buscar(p.getId());
            pDB.desconectar();
            uBD.conectar();
            u = uBD.buscar((int)session.getAttribute("login"));
            uBD.desconectar();
            request.setAttribute("name", u.getNombres() + " " + u.getApellidos());
            request.setAttribute("email", u.getEmail());
            request.setAttribute("tel", u.getTelefono());
            request.setAttribute("publicacionId", p.getId());
        } catch(SQLException e) {
            System.out.println(e.getMessage());
        }
        } else {
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);  
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DesplegarPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DesplegarPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}