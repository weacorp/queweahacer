package Controller;

import DAO.PublicacionDAOJavaBD;
import DAO.UsuarioDAOJavaDB;
import Model.Publicacion;
import Model.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ListaProyectos", urlPatterns = {"/ListaProyectos"})
public class ListaProyectos extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "catalogoProyectos.jsp";

        int res;
        String mensaje, status, elementoLista = "";
        Publicacion p;
        Usuario u = null;
        Collection<Publicacion> list;
        PublicacionDAOJavaBD pBD = new PublicacionDAOJavaBD();
        UsuarioDAOJavaDB uDB = new UsuarioDAOJavaDB();
        HttpSession session = request.getSession();
        EnviarHead h = new EnviarHead();

        try {
            pBD.conectar();
            list = (ArrayList<Publicacion>) pBD.buscar("");
            pBD.desconectar();
            if (list.isEmpty()) {
                request.setAttribute("mensaje", "Sin registros");
                request.setAttribute("status", "error");
            } else {
                Iterator pItr = list.iterator();
                while (pItr.hasNext()) {
                    p = (Publicacion) pItr.next();
                    uDB.conectar();
                    u = uDB.buscar(p.getIdUsuario());
                    uDB.desconectar();
                    elementoLista += "<li id=\"tarjetaLista\" class=\"backgroundColor textColor\">\n"
                            + "    <div class=\"tarjeta divMultipleColumn overflowHidden\">\n"
                            + "        <div class=\"titulo overflow scroller\">\n"
                            + "            <h2>" + p.getNomProyecto() + "</h2>\n"
                            + "        </div>\n"
                            + "        <div class=\"divT divMultipleRow overflowHidden\">\n"
                            + "            <div class=\"descripcion divMultipleColumn altura overflowHidden\">\n"
                            + "                <h3 class=\"tituloCuerpo\">Descripci�n del proyecto:</h3>\n"
                            + "                <div class=\"overflow scroller\">\n"
                            + "                    <p id=\"textDescripcion\" class=\"textoCuerpo\">" + p.getBrDescripcion() + "</p>\n"
                            + "                </div>\n"
                            + "            </div>\n"
                            + "            <div class=\"divDoble altura divMultipleColumn\">\n"
                            + "                <div class=\"autor \">\n"
                            + "                    <h3 class=\"tituloCuerpo maxContent\">Autor:</h3>\n"
                            + "                    <div class=\"overflow scroller autorT\">\n"
                            + "                        <p id=\"textAutor\" class=\"textoCuerpo\">" + u.getNombres() + " " + u.getApellidos() + "</p>\n"
                            + "                    </div>\n"
                            + "                </div>\n"
                            + "                <div class=\"entorno divMultipleColumn overflowHidden\">\n"
                            + "                    <h3 class=\"tituloCuerpo\">Lenguaje o entorno recomendado:</h3>\n"
                            + "                    <div class=\"overflow scroller\">\n"
                            + "                        <p id=\"textEntorno\" class=\"textoCuerpo\">" + p.getEntornoR() + "</p>\n"
                            + "                    </div>\n"
                            + "                </div>\n"
                            + "            </div>\n"
                            + "            <div class=\"contacto altura centrarVertical divMultipleColumn\">\n"
                            + "                </form>\n"
                            + "                <form class=\"verMas\" action=\"AgregarFavoritoLista\" method=\"POST\">\n"
                            + "                    <button type=\"submit\" value=\"" + p.getId() + "\" name=\"btnId\">\n"
                            + "                       <img class=\"centrarHorizontal\" src=\"images/favorito.png\" />\n"
                            //+ "                     Agregar a fav
                            + "                     </button>\n"
                            + "                </form>\n"
                            + "                <form class=\"verMas\" action=\"InformacionDeContacto\" method=\"POST\">\n"
                            + "                    <button type=\"submit\" value=\"" + p.getId() + "\" name=\"btnId\">"
                            + "                       <img class=\"centrarHorizontal\" src=\"images/contacto.png\" />\n"
                            //+ "                    Contactar
                            + "                     </button>\n"
                            + "                </form>\n"
                            + "                <form class=\"verMas\" action=\"DesplegarPublicacion\" method=\"POST\">\n"
                            + "                    <button type=\"submit\" value=\"" + p.getId() + "\" name=\"btnId\">Ver m�s...</button>\n"
                            + "                </form>\n"
                            + "            </div>\n"
                            + "        </div>\n"
                            + "    </div>\n"
                            + "</li>";
                }

                request.setAttribute("elementoLista", elementoLista);
            }
        } catch (SQLException e) {
            mensaje = "Error al conectar con el servidor";
            status = "error";
            request.setAttribute("status", status);
            request.setAttribute("mensaje", mensaje);
        }
        
        String head;
        if (session.getAttribute("login") != null) {
            head = h.pedirHead(true, session);
        } else {
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);

        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DesplegarPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(DesplegarPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
