package Controller;

import DAO.UsuarioDAOJavaDB;
import Model.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "SignUpUser", urlPatterns = {"/SignUpUser"})
public class SignUpUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        int res;
        String paginaRespuesta = "signup.jsp";
        String mensaje, status;
        Usuario u = new Usuario();
        UsuarioDAOJavaDB uDB = new UsuarioDAOJavaDB();

        u.setNombres(request.getParameter("name"));
        u.setApellidos(request.getParameter("lastname"));
        u.setEmail(request.getParameter("email"));
        u.setEmailC(request.getParameter("email-confirm"));
        u.setTelefono(request.getParameter("tel"));
        u.setTelefonoC(request.getParameter("tel-confirm"));
        u.setContrasenia(request.getParameter("password"));
        u.setContraseniaC(request.getParameter("password-confirm"));

        if (!u.getEmail().equals(u.getEmailC())) {
            status = "error";
            mensaje = "El email no coincide";
            request.setAttribute("status", status);
            request.setAttribute("mensaje", mensaje);
            rellenarDatos(request, u);
        } else {
            if (!u.getTelefono().equals(u.getTelefonoC())) {
                status = "error";
                mensaje = "El teléfono no coincide";
                request.setAttribute("status", status);
                request.setAttribute("mensaje", mensaje);
                rellenarDatos(request, u);
            } else {
                if (!u.getContrasenia().equals(u.getContraseniaC())) {
                    status = "error";
                    mensaje = "La contraseña no coincide";
                    request.setAttribute("status", status);
                    request.setAttribute("mensaje", mensaje);
                    rellenarDatos(request, u);
                } else {
                    try {
                        uDB.conectar();
                        if (uDB.existeUsuario(u, "WHERE email = '" + u.getEmail() + "'")) {
                            request.setAttribute("mensaje", "Ya existe un usuario con el email " + u.getEmail());
                            request.setAttribute("status", "error");
                            rellenarDatos(request, u);
                        } else {
                            res = uDB.agregar(u);
                            if (res == 0) {
                                mensaje = "Error al conectar con el servidor";
                                request.setAttribute("status", "error");
                                request.setAttribute("mensaje", mensaje);
                                rellenarDatos(request, u);
                            } else {
                                HttpSession session = request.getSession();
                                u = uDB.autorizarLogIn(u);
                                session.setAttribute("login", u.getId());
                                session.setAttribute("user", u.getNombres() + " " + u.getApellidos());
                                session.setAttribute("email", u.getEmail());
                                paginaRespuesta = "index.jsp";
                            }
                        }
                    } catch (SQLException ex) {
                        request.setAttribute("status", "error");
                        request.setAttribute("mensaje", "Error en el servidor");
                        rellenarDatos(request, u);
                    }
                }
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }

    private void rellenarDatos(HttpServletRequest request, Usuario u) {
        request.setAttribute("name", u.getNombres());
        request.setAttribute("lastname", u.getApellidos());
        request.setAttribute("email", u.getEmail());
        request.setAttribute("emailC", u.getEmailC());
        request.setAttribute("tel", u.getTelefono());
        request.setAttribute("telC", u.getTelefonoC());
        request.setAttribute("pass", u.getContrasenia());
        request.setAttribute("passC", u.getContraseniaC());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
