package Controller;

import DAO.UsuarioDAOJavaDB;
import Model.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "LogIn", urlPatterns = {"/LogIn"})
public class LogIn extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");

        String paginaRespuesta = "login.jsp";
        String mensaje, email, password;
        Usuario u = new Usuario();
        UsuarioDAOJavaDB uDB = new UsuarioDAOJavaDB();

        email = request.getParameter("email");
        password = request.getParameter("password");

        u.setEmail(email);
        u.setContrasenia(password);

        try {
            uDB.conectar();
            u = uDB.autorizarLogIn(u);
            if (u.getId() != 0) {
                HttpSession session = request.getSession();
                session.setAttribute("login", u.getId());
                session.setAttribute("user", u.getNombres()+" "+u.getApellidos());
                session.setAttribute("email", u.getEmail());
                paginaRespuesta = "ListaProyectos";
            } else {
                request.setAttribute("mensaje", "Error en las credenciales");
                request.setAttribute("status", "error");
            }
        } catch (SQLException ex) {
            mensaje = "Servicio no disponible, contacte con su Administrador";
            request.setAttribute("status", "error");
            request.setAttribute("mensaje", mensaje+"\n"+ex);
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LogIn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LogIn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
