package Controller;

import Model.Favorito;
import DAO.FavoritoDAOJavaDB;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "EliminarFavoritoLista", urlPatterns = {"/EliminarFavoritoLista"})
public class EliminarFavoritoLista extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        
        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "ListaFavoritos";
        EnviarHead h = new EnviarHead();
        String head;
        
        int res = 0;
        Favorito f = new Favorito();
        Collection<Favorito> list;
        FavoritoDAOJavaDB fDB = new FavoritoDAOJavaDB();
        HttpSession session = request.getSession();

        if (session.getAttribute("login") != null) {
            int idUser = (int) session.getAttribute("login");
            head = h.pedirHead(true, session);
        f.setAutorId(idUser);
        f.setPublicacionId(Integer.parseInt(request.getParameter("btnId")));
        
        try {
            fDB.conectar();
            list = fDB.buscar("WHERE usuarioid = " + f.getAutorId() + " AND publicacionid = " + f.getPublicacionId());
            
            Iterator pItr = list.iterator();
            while (pItr.hasNext())
                f = (Favorito) pItr.next();
            res = fDB.eliminar(f.getFavoritoId());
            fDB.desconectar();
            if (res == 0) {
                request.setAttribute("status", "error");
                request.setAttribute("mensaje", "Error al borrar favorito");
            } else {
                request.setAttribute("status", "correcto");
                request.setAttribute("mensaje", "Favorito borrado con exito");
            }
        } catch (SQLException e) {
            request.setAttribute("status", "error");
            request.setAttribute("mensaje", "Error en el servidor " + e.getMessage());
        }
        } else {
            paginaRespuesta = "login.jsp";
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    } 
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
