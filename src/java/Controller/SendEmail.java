package Controller;

import DAO.PublicacionDAOJavaBD;
import DAO.UsuarioDAOJavaDB;
import Model.InfoContacto;
import Model.Publicacion;
import Model.Usuario;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "SendEmail", urlPatterns = {"/SendEmail"})
public class SendEmail extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "informacionContacto.jsp";
        
        String remitterS = "queweahacer@gmail.com";
        String clave = "2aaeeNm8EUTRtAZ";
        String asunto = "queWEAhacer",
               mensaje = "Tienes una propuesta de desarrollo",
               destinatario = "daniel_bueno.be";

        String host = "smtp.gmail.com";

        Publicacion p;
        Usuario u = new Usuario();
        InfoContacto infoC = new InfoContacto();
        PublicacionDAOJavaBD pBD = new PublicacionDAOJavaBD();
        UsuarioDAOJavaDB uDB = new UsuarioDAOJavaDB();
        HttpSession session = request.getSession();
        EnviarHead h = new EnviarHead();
        String head;
        if (session.getAttribute("login") != null) {
            head = h.pedirHead(true, session);

        infoC.setNombres(request.getParameter("name"));
        infoC.setEmail(request.getParameter("email"));
        infoC.setTelefono(request.getParameter("tel"));
        infoC.setSkype(request.getParameter("skype"));
        infoC.setGitHub(request.getParameter("github"));
        infoC.setLinkenid(request.getParameter("linkenid"));
        infoC.setMessage(request.getParameter("message"));
        if (request.getParameter("btnId") != null &&
                !"".equals(request.getParameter("btnId")))
            infoC.setPublicaciobID(Integer.parseInt(request.getParameter("btnId")));

        try {
            pBD.conectar();
            p = pBD.buscar(infoC.getPublicaciobID());
            pBD.desconectar();
            uDB.conectar();
            u = uDB.buscar(p.getIdUsuario());
            uDB.desconectar();
            // conseguimos el destinatario
            destinatario = u.getEmail();

            asunto = "[queWEAhacer] " + asunto;
            mensaje = "Hey " + u.getNombres() + " " + u.getApellidos()  + "!<br><br>" +
                      "Tienes una propuesta nueva para desarrollar tu proyecto<br><br>" +
                      infoC.getNombres() + " está interesado en: <b>"  + p.getNomProyecto() + "</b><br>" +
                      "Información de contacto: <br>" +
                      "<ul>";
                      if (infoC.getEmail() != null)
                          mensaje += "<li>" + infoC.getEmail() + "</li>";
                      if (infoC.getTelefono() != null)
                          mensaje += "<li>" + infoC.getTelefono() + "</li>";
                      if (infoC.getSkype() != null)
                          mensaje += "<li>" + infoC.getSkype()+ "</li>";
                      if (infoC.getGitHub()!= null)
                          mensaje += "<li>" + infoC.getGitHub()+ "</li>";
                      if (infoC.getLinkenid() != null)
                          mensaje += "<li>" + infoC.getLinkenid() + "</li>";
                      mensaje += "</ul>";
                      mensaje += infoC.getNombres() + "<b> dice:</b>" +
                                 "<p>" + infoC.getMessage()+ "</p>" +
                                 "<br><br>Gracias, <br> queWEAhacer Team";
        } catch(Exception ex) {
            request.setAttribute("status", "error");
            request.setAttribute("mensaje", "Ah ocurrido un error al conseguir "
                    + "la información de contacto " + ex.getMessage());
            paginaRespuesta = "InformacionDeContacto";
        }

        try {
            InternetAddress remitter = new InternetAddress(remitterS);
            /* session object */
            Properties prop = System.getProperties();
            prop.put("mail.smtp.host", "smtp.gmail.com");
            prop.put("mail.smtp.socketFactory.port", "465");
            prop.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory");
            prop.put("mail.smtp.auth", "true");
            prop.put("mail.smtp.port", "465");
            prop.put("mail.smtp.starttls.enable", "true");
            prop.put("mail.smtp.ssl.trust", "smtp.gmail.com");

            Session sesion = Session.getDefaultInstance(prop,null);

            /* compose message */
            MimeMessage message = new MimeMessage(sesion);
            message.setFrom(remitter);
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            message.setSubject(asunto);
            message.setContent(mensaje, "text/html; charset=utf-8");
            
            /* send message */
            try (Transport transport = sesion.getTransport("smtp")) {
                transport.connect(host,remitterS,clave);
                transport.sendMessage(message, message.getAllRecipients());
            }
            
            request.setAttribute("status", "correcto");
            request.setAttribute("mensaje", "Envio de mail correctamente <a href=\"ListaProyectos\"> </a> ");
            rellenarDatos(request, infoC);
        } catch(MessagingException mex) {
            request.setAttribute("status", "error");
            request.setAttribute("mensaje", "Un error ah ocurrido en el envio de mail " + mex.getMessage());
            rellenarDatos(request, infoC);
        }
        } else {
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }
    
    private void rellenarDatos(HttpServletRequest request, InfoContacto infoC) {
        request.setAttribute("name", infoC.getNombres());
        request.setAttribute("email", infoC.getEmail());
        request.setAttribute("tel", infoC.getTelefono());
        request.setAttribute("skype", infoC.getSkype());
        request.setAttribute("github", infoC.getGitHub());
        request.setAttribute("linkenid", infoC.getLinkenid());
        request.setAttribute("message", infoC.getMessage());
        request.setAttribute("publicacionId", infoC.getPublicaciobID());
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}