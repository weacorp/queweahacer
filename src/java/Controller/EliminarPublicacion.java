package Controller;

import DAO.PublicacionDAOJavaBD;
import Model.Publicacion;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "EliminarPublicacion", urlPatterns = {"/EliminarPublicacion"})
public class EliminarPublicacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "MisPublicaciones";

        int res = 0;
        Publicacion p = new Publicacion();
        PublicacionDAOJavaBD pBD = new PublicacionDAOJavaBD();

        p.setId(Integer.parseInt(request.getParameter("btnId")));
        HttpSession session = request.getSession();
        String head;
        EnviarHead h = new EnviarHead();
        
        if (session.getAttribute("login") != null) {
            head = h.pedirHead(true, session);

            try {
                pBD.conectar();
                res = pBD.eliminar(p.getId());

                if (res == 0) {
                    request.setAttribute("status", "error");
                    request.setAttribute("mensaje", "Error al borrar publicación");
                } else {
                    request.setAttribute("status", "correcto");
                    request.setAttribute("mensaje", "Publicación borrada exitosa");
                }
            } catch (SQLException e) {
                request.setAttribute("status", "error");
                request.setAttribute("mensaje", "Error en el servidor " + e.getMessage());
            }
        } else {
            paginaRespuesta = "login.jsp";
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);

        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
