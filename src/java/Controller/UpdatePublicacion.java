package Controller;

import DAO.PublicacionDAOJavaBD;
import Model.Publicacion;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "UpdatePublicacion", urlPatterns = {"/UpdatePublicacion"})
public class UpdatePublicacion extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "updatePublicacion.jsp";
        
        int res = 0;
        Publicacion p = new Publicacion();
        PublicacionDAOJavaBD pBD = new PublicacionDAOJavaBD();
        HttpSession session = request.getSession();
        String head;
        EnviarHead h = new EnviarHead();

        if (session.getAttribute("login") != null) {
            int idUser = (int) session.getAttribute("login");
            head = h.pedirHead(true, session);
        
        p.setCategoria(request.getParameter("categoriaX")); //categoriaX
        p.setNomProyecto(request.getParameter("nombreProyecto"));
        p.setEstDesarrollo(request.getParameterValues("estadoDesarrollo")); //estadoDesarrollo: idea, disenio, especificaciones, nada
        p.setBrDescripcion(request.getParameter("descripcion"));
        p.setLenguajeR(request.getParameter("lenguaje"));
        p.setEntornoR(request.getParameter("entorno"));
        p.setIntApi(request.getParameterValues("APIs")); //APIs: social, payment, cloudS, otros
        p.setPlzDias(Integer.valueOf(request.getParameter("entregaDias")));
        p.setAdjArchivos(request.getParameter("adjuntarArchivos"));
        p.setId(Integer.parseInt(request.getParameter("btnId")));
        p.setIdUsuario(idUser);

        
        if (p.getEstDesarrollo() != null && p.getIntApi() != null) {
            try {
                pBD.conectar();
                res = pBD.modificar(p);

                if (res == 0) {
                    request.setAttribute("status", "error");
                    request.setAttribute("mensaje", "Error al actualizar datos");
                    rellenarDatos(request, p);
                } else {
                    request.setAttribute("status", "correcto");
                    request.setAttribute("mensaje", "Actualización exitosa");
                    paginaRespuesta = "MisPublicaciones";
                }
            } catch (SQLException e) {
                request.setAttribute("status", "error");
                request.setAttribute("mensaje", "Error en el servidor " + e.getMessage());
                rellenarDatos(request, p);
            }
        } else {
            request.setAttribute("status", "error");
            request.setAttribute("mensaje", "Favor de completetar los campos");
            rellenarDatos(request, p);
        }
        } else {
            paginaRespuesta = "login.jsp";
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }
    
    private void rellenarDatos(HttpServletRequest request, Publicacion p) {
        request.setAttribute("userId", p.getId());
        request.setAttribute("nombreProyecto", p.getNomProyecto());
        request.setAttribute("descripcion", p.getBrDescripcion());
        request.setAttribute("entorno", p.getEntornoR());
        request.setAttribute("lenguaje", p.getLenguajeR());
        request.setAttribute("entregaDias", p.getPlzDias());
        request.setAttribute("btnId", p.getId());
        request.setAttribute(p.getCategoria().substring(0, 1), "selected");
        if (request.getParameterValues("APIs") != null)
            for (String a : request.getParameterValues("APIs"))
                request.setAttribute(a.substring(0, 2), "checked");
        if (request.getParameterValues("estadoDesarrollo") != null)
            for (String a : request.getParameterValues("estadoDesarrollo"))
                request.setAttribute(a.substring(a.length() - 1, a.length()), "checked");
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}