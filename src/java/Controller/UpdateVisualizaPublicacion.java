package Controller;

import DAO.PublicacionDAOJavaBD;
import Model.Publicacion;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "UpdateVisualizaPublicacion", urlPatterns = {"/UpdateVisualizaPublicacion"})
public class UpdateVisualizaPublicacion extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        String paginaRespuesta = "updatePublicacion.jsp";

        Publicacion p = new Publicacion();
        PublicacionDAOJavaBD pBD = new PublicacionDAOJavaBD();
        HttpSession session = request.getSession();
        String head;
        EnviarHead h = new EnviarHead();

        if (session.getAttribute("login") != null) {
            head = h.pedirHead(true, session);

            p.setId(Integer.parseInt(request.getParameter("btnId")));

            try {
                pBD.conectar();
                p = pBD.buscar(p.getId()); // busca por id : test by 1
                if (p != null) {
                    request.setAttribute("userId", p.getId());
                    request.setAttribute("nombreProyecto", p.getNomProyecto());
                    switch (p.getCategoria()) {
                        case ("Programación WEB"):
                            request.setAttribute("val1", "selected");
                            break;
                        case ("Dispositivos Moviles"):
                            request.setAttribute("val2", "selected");
                            break;
                        case ("Inteligencia Artifical"):
                            request.setAttribute("val3", "selected");
                            break;
                        case ("Base de Datos"):
                            request.setAttribute("val4", "selected");
                            break;
                    }
                    request.setAttribute("descripcion", p.getBrDescripcion());
                    if (p.getEstDesarrollo() != null)
                        for (String a : p.getEstDesarrollo())
                            request.setAttribute(a.substring(a.length() - 1, a.length()), "checked");
                    if (p.getIntApi() != null)
                        for (String a : p.getIntApi())
                            request.setAttribute(a.trim().substring(0, 2), "checked");
                    request.setAttribute(p.getCategoria().substring(0, 1), "selected");
                    request.setAttribute("entorno", p.getEntornoR());
                    request.setAttribute("lenguaje", p.getLenguajeR());
                    request.setAttribute("entregaDias", p.getPlzDias());
                    request.setAttribute("btnId", p.getId()); 
                } else {
                    request.setAttribute("status", "error");
                    request.setAttribute("mensaje", "Error al cargar los datos");
                }
            } catch (SQLException e) {
                request.setAttribute("status", "error");
                request.setAttribute("mensaje", "Error en el servidor");
            }
        } else {
            paginaRespuesta = "login.jsp";
            head = h.pedirHead(false, session);
        }
        request.setAttribute("head", head);

        RequestDispatcher dispatcher = request.getRequestDispatcher(paginaRespuesta);
        dispatcher.forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(NuevaPublicacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
