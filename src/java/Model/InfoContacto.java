package Model;

public class InfoContacto {
    int publicaciobID;
    private String nombres, email, telefono, skype, gitHub, linkenid, message;

    public int getPublicaciobID() {
        return publicaciobID;
    }

    public void setPublicaciobID(int publicaciobID) {
        this.publicaciobID = publicaciobID;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getLinkenid() {
        return linkenid;
    }

    public void setLinkenid(String linkenid) {
        this.linkenid = linkenid;
    }

    public String getGitHub() {
        return gitHub;
    }

    public void setGitHub(String gitHub) {
        this.gitHub = gitHub;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}