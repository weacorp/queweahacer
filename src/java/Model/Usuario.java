package Model;

public class Usuario {

    private int id;
    private String nombres, apellidos, email, emailC, telefono, telefonoC, contrasenia, contraseniaC;
    
    public int getId() {
        return id;
    }

    public String getNombres() {
        return nombres;
    }
    
    public String getApellidos() {
        return apellidos;
    }

    public String getEmail() {
        return email;
    }
    
    public String getEmailC() {
        return emailC;
    }
    
    public String getTelefono() {
        return telefono;
    }
    
    public String getTelefonoC() {
        return telefonoC;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public String getContraseniaC() {
        return contraseniaC;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public void setEmailC(String emailC) {
        this.emailC = emailC;
    }

    public void setTelefonoC(String telefonoC) {
        this.telefonoC = telefonoC;
    }

    public void setContraseniaC(String contraseniaC) {
        this.contraseniaC = contraseniaC;
    }



}