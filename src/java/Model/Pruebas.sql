/*DROP TABLE PUBLICACION;*/
/*
CREATE TABLE "PUBLICACION" (
    "ID" INT not null primary key
        GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
    "CATEGORIA" VARCHAR(50),
    "NOMPROYECTO" VARCHAR(500),
    "ESTDESARROLLO" VARCHAR(60),
    "BRDESCRIPCION" VARCHAR(2500),
    "LENGUAJER" VARCHAR(500),
    "ENTORNOR" VARCHAR(500),
    "INTAPI" VARCHAR(60),
    "PLZDIAS" SMALLINT,
    "IDUSUARIO" INT,
    "FAVORITOS" INT,
    "ADJARCHIVOS" VARCHAR(1500)
);
*//*
INSERT INTO PUBLICACION (categoria, nomProyecto, estDesarrollo, brDescripcion, lenguajeR, entornoR, intApi, plzDias, IDUSUARIO, favoritos, adjArchivos)
                 VALUES ('Prog Web', 'Proyect 1', 'tengo la idea', 'Descripto', 'prolg', 'Atom', 'Payment', 100, 2, 2, 'null');

UPDATE PUBLICACION SET
                "CATEGORIA" = 'Prolog',
                "NOMPROYECTO" = 'proyecto 7',
                "ESTDESARROLLO" = 'la idea',
                "BRDESCRIPCION" = 'descripto 2',
                "LENGUAJER" = 'Prolog, Python',
                "ENTORNOR" = 'JQuery, Bootstrap',
                "INTAPI" = 'nada',
                "PLZDIAS" = 92,
                "IDUSUARIO" = 1,
                "FAVORITOS" = 1
                WHERE "ID" = 2;
*/
SELECT * FROM PUBLICACION;
/*DROP TABLE PUBLICACION;*/
SELECT * FROM USUARIO;
/*
INSERT INTO USUARIO (NOMBRES, APELLIDOS, EMAIL, TELEFONO, CONTRASENIA)
                 VALUES ('daniel', 'bueno', 'mail3@mail.com', '4771234567', '123');

SELECT * FROM USUARIO;
/*
PUBLICACION
USUARIO
FAVORITOS
*/

SELECT * FROM FAVORITOS WHERE usuarioid = 1 AND publicacionid = 7
