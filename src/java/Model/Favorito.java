package Model;

public class Favorito {
    int favoritoId, autorId, publicacionId;

    public int getFavoritoId() {
        return favoritoId;
    }

    public void setFavoritoId(int favoritoId) {
        this.favoritoId = favoritoId;
    }

    public int getAutorId() {
        return autorId;
    }

    public void setAutorId(int autorId) {
        this.autorId = autorId;
    }

    public int getPublicacionId() {
        return publicacionId;
    }

    public void setPublicacionId(int publicacionId) {
        this.publicacionId = publicacionId;
    }
}