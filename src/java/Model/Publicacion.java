package Model;

public class Publicacion {
    int id, plzDias, idUsuario, favorito;
    String[] estDesarrollo, intApi;
    String categoria, nomProyecto, brDescripcion,
               lenguajeR, entornoR, adjArchivos;
    
    public int getIdUsuario() {
        return idUsuario;
    }
    
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNomProyecto() {
        return nomProyecto;
    }

    public void setNomProyecto(String nomProyecto) {
        this.nomProyecto = nomProyecto;
    }

    public String[] getEstDesarrollo() {
        return estDesarrollo;
    }

    public void setEstDesarrollo(String[] estDesarrollo) {
        this.estDesarrollo = estDesarrollo;
    }

    public String getBrDescripcion() {
        return brDescripcion;
    }

    public void setBrDescripcion(String brDescripcion) {
        this.brDescripcion = brDescripcion;
    }

    public String getLenguajeR() {
        return lenguajeR;
    }

    public void setLenguajeR(String lenguajeR) {
        this.lenguajeR = lenguajeR;
    }

    public String getEntornoR() {
        return entornoR;
    }

    public void setEntornoR(String entornoR) {
        this.entornoR = entornoR;
    }

    public String[] getIntApi() {
        return intApi;
    }

    public void setIntApi(String[] intApi) {
        this.intApi = intApi;
    }

    public int getPlzDias() {
        return plzDias;
    }

    public void setPlzDias(int plzDias) {
        this.plzDias = plzDias;
    }
    
    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public String getAdjArchivos() {
        return adjArchivos;
    }

    public void setAdjArchivos(String adjArchivos) {
        this.adjArchivos = adjArchivos;
    }
    
    public String armaStringListaChecks(String[] lista) {
        String t = "";
        for (String l : lista)
                t = t + l + ", ";
        return t.substring(0, t.trim().length()-1); // bueno, sirve por el momento
    }
    
    public String[] splitListaChecks(String lista) {
        return lista.split(",");
    }
    
    public String getTodo(){
        return categoria + ", " + nomProyecto + ", [" + armaStringListaChecks(estDesarrollo) + "], " + brDescripcion + ", " +
               lenguajeR + ", " + entornoR + ", [" + armaStringListaChecks(intApi) + "], " + plzDias + ", " + adjArchivos;
    }
    
//    public static void main(String[] args) {
//        Publicacion p = new Publicacion();
//        System.out.println(p.splitListaChecks("hola, como, esta"));
//        System.out.println(p.armaStringListaChecks(p.splitListaChecks("hola, como, esta")));
//    }
}