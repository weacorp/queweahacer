/*
Datos para creación de BD
    Nombre: queWEAhacer
    Usuario: root
    Contraseña: root

-- ----------------------------------------------------------------

NOTA: En Java DB no es posible crear la BD a aprtir de codigo SQL
-- Crear BD
CREATE DATABASE queWEAhacer;

-- Crear Usuario y Login
CREATE LOGIN root WITH PASSWORD = 'root';
CREATE USER root FOR LOGIN root;
*/
-- ----------------------------------------------------------------
-- Crear la tabla
/*DROP TABLE USUARIO;*/
CREATE TABLE "USUARIO" (
    "ID" INT not null primary key
        GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
    "NOMBRES" VARCHAR(50),
    "APELLIDOS" VARCHAR(70),
    "EMAIL" VARCHAR(50),
    "TELEFONO" VARCHAR(13),
    "CONTRASENIA" VARCHAR(50)
);

DROP TABLE PUBLICACION;
CREATE TABLE "PUBLICACION" (
    "ID" INT not null primary key
        GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
    "CATEGORIA" VARCHAR(50),
    "NOMPROYECTO" VARCHAR(500),
    "ESTDESARROLLO" VARCHAR(110),
    "BRDESCRIPCION" VARCHAR(2500),
    "LENGUAJER" VARCHAR(500),
    "ENTORNOR" VARCHAR(500),
    "INTAPI" VARCHAR(110),
    "PLZDIAS" SMALLINT,
    "IDUSUARIO" INT,
    "ADJARCHIVOS" VARCHAR(1500)
);

DROP TABLE FAVORITOS;
CREATE TABLE "FAVORITOS" (
    "ID" INT not null primary key
        GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
    "USUARIOID" INT,
    "PUBLICACIONID" INT
);